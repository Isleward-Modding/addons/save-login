# Save Login

This addon will auto fill your login details.

Useful when testing mods/addons for a faster login.

To use just change the example username and password values to your own.
If you would not like to enter your password for security reasons just leave its value blank (eg. "")
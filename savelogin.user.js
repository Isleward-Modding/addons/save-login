// ==UserScript==
// @name         Save Login
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       GoogzGazuli
// @match        https://play.isleward.com/*
// @grant        none
// ==/UserScript==
var username = "ExampleUsername"
var password = "ExamplePassword"

function checkUiLoaded(ui_name) {
    if ($(ui_name)[0]){
        $(".txtUsername").val(username);
        $(".txtPassword").val(password);
    } else {
        setTimeout(function() { checkUiLoaded(ui_name) }, 50);
    }
}

function defer(method) {
    if (window.jQuery) {
        method();
    } else {
        setTimeout(function() { defer(method) }, 50);
    }
}

defer(
(function () {
    addons.register({
        init: function(events) {
            events.on('onResourcesLoaded', this.onResourcesLoaded.bind(this));
        },
        onResourcesLoaded: function() {
            checkUiLoaded(".btnLogin");
        },
    });
}));
